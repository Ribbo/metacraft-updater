import requests
import json
import os
import logging
import sys
import threading
import hashlib
import argparse
import time
import bz2


logging.basicConfig(level=logging.DEBUG, format="(%(threadName)-4s) %(message)s")
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


pf = logging.log
dbg = logging.DEBUG

craftURI = "http://minecraft.xerasin.com/downloadmeta.php"
mcjson = []
oPath = ""

pyload = {"User-Agent": "Googlebot/2.1 (+http://www.googlebot.com/bot.html)",}


class DLThread(threading.Thread):
    
    global oPath

    def __init__(self, uri, hash, jPath):
        threading.Thread.__init__(self, group=None, target=None, name=None, args=(uri,hash,jPath), kwargs=None)
        self.bigPath = oPath
        self.file = uri
        self.fhash = hash
        self.jsonPath = jPath

        self.storePath = ""
        return

    def run(self):
        logging.log(logging.DEBUG, "New thread spawned\n")
    
        fPath = self.getpath(self.jsonPath)
        if(fPath == None):
            pf(dbg, "Fatal error, getpath = None")
            return

        self.pathexists(fPath)

        self.download_file()

        self.decompress_file()

        self.delete_oldfile()

        return

    def pathexists(self,path):
        global oPath
        self.bigPath = self.bigPath + path
        try:
            if(os.path.exists(self.bigPath)):
                logging.log(logging.DEBUG, "Path {0} exists".format(self.bigPath))
                return True
            else:
                logging.log(logging.DEBUG, "Path {0} doesn't exist, creating...".format(self.bigPath))
                os.makedirs(self.bigPath)
                self.storePath = self.bigPath
        except Exception as e:
            pass


    def getpath(self, uri):
        if(type(uri) == str):
            uri = uri.split("/")
            uri.pop(len(uri)-1)
            nUri = ""
            for x in range(0,len(uri),1):
                nUri = nUri + "/" + uri[x]

            return nUri
        return None

    def format_fname(self,fname):
        if(type(fname) == str):
            an = fname.split("/")[-1]
            return an

        return None
        
    
    def download_file(self):
        fname = self.format_fname(self.file)
        freq = requests.get(self.file, allow_redirects=False, headers=pyload)
        try:
            with open(str(self.bigPath + "/" + fname), "wb") as f:
                for chunk in freq.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)

            pf(dbg, "Successfully downloaded {0}!".format(fname))
            return
        except Exception as e:
            pf(dbg, "Error in downloading the file {0}, retrying...\n".format(fname))
            print(e)
            self.download_file()
        

    def decompress_file(self):
        zfile = bz2.BZ2File(self.bigPath + "/" + self.format_fname(self.file))
        nfpath = self.format_fname(self.file)[:-4]
        try:
            with open(self.bigPath + "/" + nfpath, "wb") as myf:
                myf.write(zfile.read())

            pf(dbg, "Successfully decompressed {0}!".format(nfpath))
        except Exception as e:
            pf(dbg, "Error in decompress_file {0}.".format(str(e))) 
            return
    
    
    def delete_oldfile(self):
        ofile = self.format_fname(self.file)
        try:
            if(os.path.isfile(self.bigPath + "/" + ofile)):
                os.remove(self.bigPath + "/" + ofile)

        except Exception as e:
            pf(dbg, "Error deleting old file!")
            return


def main(): 
    global mcjson
    global oPath

    if not os.path.exists("files"):
         os.mkdir("files")
    oPath = "files"
   

    try:
        a = requests.get(craftURI)
        if a.text is not None or a.text is not "":
            pf(dbg, "Got craftURI, let's go!")

        
        if(a):
            mcjson = json.loads(a.text)


        for x in range(0, len(mcjson["files"]), 1):
            t = DLThread(mcjson["files"][x]["url"], mcjson["files"][x]["hash"], mcjson["files"][x]["path"])
            t.start()
            time.sleep(0.15)

    except Exception as e:
        logging.log(logging.DEBUG, str(e))
    


main()
